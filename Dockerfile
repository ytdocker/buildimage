FROM python:3

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y \
        jq \
        unzip \
        curl \
        git \
        apt-transport-https ca-certificates software-properties-common gnupg2 \
    && pip install --upgrade pip \
    && pip install kubernetes
#        build-essential \ 

# install Docker
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
    && apt-get update && apt-get install -y docker-ce \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# install Docker-Compose
RUN lastver=$(curl https://api.github.com/repos/docker/compose/releases/latest | jq --raw-output '.tag_name') \
    && curl -L "https://github.com/docker/compose/releases/download/${lastver}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

# install Terraform
ENV version_terraform 0.14.2
ADD https://releases.hashicorp.com/terraform/${version_terraform}/terraform_${version_terraform}_linux_amd64.zip /root/terraform.zip
RUN cd /root \
    && unzip terraform.zip \
    && rm terraform.zip \
    && mv terraform /bin/

#ADD get-latest-terraform.sh /root/get-latest-terraform.sh
#RUN cd /root && chmod +x ./get-latest-terraform.sh && ./get-latest-terraform.sh

